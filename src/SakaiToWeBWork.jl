module SakaiToWeBWork

export readstu, writestu, readwritestu, stu2table

using CSV, DataFrames

struct Student
    name::String
    netid::String
    email::String
    role::String
end

function readstu(fname)
    return CSV.read(fname, DataFrame)
end

function stu2table(stu)
    tbl = Vector{Vector{String}}()

    for s in eachrow(stu)
        # student_id    last_name   first_name  status  comment     section
        # recitation    email_address   user_id     password (crypted)  permission
        (lastname, firstname) = split(s.Name, ", ")
        id = s.NetID
        role = "0"
        row = String[id, lastname, firstname, "C", "", "", 
                     "", s["Email Address"], id, "", role]
        @show tbl, row
        push!(tbl, row)
    end

    tbl = mapreduce(permutedims, vcat, tbl)
    return Tables.table(tbl)
end

function writestu(stu, fname)

    return CSV.write(fname, stu2table(stu), writeheader=false)
end

function readwritestu(input, output)
    stu = readstu(input)
    writestu(stu, output)
end

end
